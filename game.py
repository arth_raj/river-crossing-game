import pygame
import math
from config import *
pygame.init()
win=pygame.display.set_mode((1200,770))
pygame.display.set_caption("First Game")
char = pygame.image.load('standing.png')
char1=pygame.image.load('standing1.png')
ship=pygame.image.load('ship.jpg')
snake=pygame.image.load('snake.jpg')
rock=pygame.image.load('rock.png')
clock=pygame.time.Clock()
cfont=pygame.font.SysFont('comicsans',90,True,True)
efont=pygame.font.SysFont('comicsans',40,True,True)


def bgwin():
    pygame.draw.rect(win, (188,59,59), (0,0,900,40))
    pygame.draw.rect(win, (0,0,255), (0,40,900,100))
    pygame.draw.rect(win, (188,59,59), (0,140,900,40))
    pygame.draw.rect(win, (0,0,255), (0,180,900,100))
    pygame.draw.rect(win, (188,59,59), (0,280,900,40))
    pygame.draw.rect(win, (0,0,255), (0,320,900,100))
    pygame.draw.rect(win, (188,59,59), (0,420,900,40))
    pygame.draw.rect(win, (0,0,255), (0,460,900,100))
    pygame.draw.rect(win, (188,59,59), (0,560,900,40))
    pygame.draw.rect(win, (0,0,255), (0,600,900,100))
    pygame.draw.rect(win, (188,59,59), (0,700,900,70))
    win.blit(snake,(10,140))
    win.blit(snake,(110,140))
    win.blit(snake,(310,140))
    win.blit(snake,(410,140))
    win.blit(snake,(610,140))
    win.blit(snake,(790,140))
    win.blit(rock,(20,280))
    win.blit(rock,(170,280))
    win.blit(rock,(320,280))
    win.blit(rock,(470,280))
    win.blit(rock,(620,280))
    win.blit(rock,(770,280))
    win.blit(snake,(40,420))
    win.blit(snake,(140,420))
    win.blit(snake,(390,420))
    win.blit(snake,(610,420))
    win.blit(snake,(800,420))
    win.blit(rock,(20,560))
    win.blit(rock,(170,560))
    win.blit(rock,(320,560))
    win.blit(rock,(470,560))
    win.blit(rock,(620,560))
    win.blit(rock,(770,560))
    pygame.draw.rect(win, (172,159,159), (900,0,300,770))


def isCollision(x1,y1,x2,y2,l):
    dist=math.sqrt(math.pow((x2-x1),2)+math.pow((y2-y1),2))
    if dist < l:
        return True


def dcws1(sx,sy):
    global fplayer1,c,t1
    if isCollision(man1.x,man1.y,sx,sy,50) == True:
        man1.x=10
        man1.y=700
        fplayer1=False
        text=cfont.render('WASTED :(',1,(255,0,0))
        win.blit(text,(300,350))
        pygame.display.update()
        pygame.time.delay(2000)
        t1=-2
        c=1


def dcwr1(sx,sy):
    global fplayer1,c,t1
    if isCollision(man1.x,man1.y,sx,sy,25) == True:
        man1.x=10
        man1.y=700
        fplayer1=False
        text=cfont.render('WASTED :(',1,(255,0,0))
        win.blit(text,(300,350))
        pygame.display.update()
        pygame.time.delay(2000)
        t1-=2
        c=1


def dcwb1(sx,sy):
    global fplayer1,c,t1
    if isCollision(man1.x,man1.y,sx,sy,30) == True:
        man1.x=10
        man1.y=700
        fplayer1=False
        text=cfont.render('WASTED :(',1,(255,0,0))
        win.blit(text,(300,350))
        pygame.display.update()
        pygame.time.delay(2000)
        t1-=2
        c=1


def dcws2(sx,sy):
    global fplayer2,c,t2
    if isCollision(man2.x,man2.y,sx,sy,50) == True:
        man2.x=800
        man2.y=-10
        fplayer2=False
        text=cfont.render('WASTED :(',1,(255,0,0))
        win.blit(text,(300,350))
        pygame.display.update()
        pygame.time.delay(2000)
        t2-=2
        c=0


def dcwr2(sx,sy):
    global fplayer2,c,t2
    if isCollision(man2.x,man2.y,sx,sy,25) == True:
        man2.x=800
        man2.y=-10
        fplayer2=False
        text=cfont.render('WASTED :(',1,(255,0,0))
        win.blit(text,(300,350))
        pygame.display.update()
        pygame.time.delay(2000)
        t2-=2
        c=0


def dcwb2(sx,sy):
    global fplayer2,c,t2
    if isCollision(man2.x,man2.y,sx,sy,30) == True:
        man2.x=800
        man2.y=-10
        fplayer2=False
        text=cfont.render('WASTED :(',1,(255,0,0))
        win.blit(text,(300,350))
        pygame.display.update()
        t2-=2
        pygame.time.delay(2000)
        c=0


def scoredisplay():
    global rounds
    text1=font.render('Score player 1: '+str(man1.scoreplayer),1,(0,0,0))
    win.blit(text1,(950,100))
    text2=font.render('Score player 2: '+str(man2.scoreplayer),1,(0,0,0))
    win.blit(text2,(950,400))
    text3=cfont.render('Round '+str(rounds+1),1,(0,0,0))
    win.blit(text3,(904,650))


def timedisplay():
    global t1,t2
    text1=font.render('Time taken: '+str('%0.1f'%(t1/1000)),1,(0,0,0))
    win.blit(text1,(950,250))
    text2=font.render('Time taken: '+str('%0.1f'%(t2/1000)),1,(0,0,0))
    win.blit(text2,(950,550))


def summarywindow():
    global t1,t2
    bfont=pygame.font.SysFont('comicsans',60,True,True)
    pygame.draw.rect(win, (97,250,240), (0,0,1200,770))
    te1=bfont.render('Game Summary',1,(0,0,0))
    win.blit(te1,(400,100))
    te2=bfont.render('Total points of player 1: '+str(man1.scoreplayer),1,(0,0,0))
    win.blit(te2,(200,200))
    te3=bfont.render('Total points of player 2: '+str(man2.scoreplayer),1,(0,0,0))
    win.blit(te3,(200,300))
    te4=bfont.render('Time taken by player 1: '+str('%0.1f'%(t1/1000)),1,(0,0,0))
    win.blit(te4,(200,400))
    te6=bfont.render('Time taken by player 2: '+str('%0.1f'%(t2/1000)),1,(0,0,0))
    win.blit(te6,(200,500))
    
    
    if (man1.scoreplayer>man2.scoreplayer):
        te5=bfont.render('Result:Player 1 wins!!',1,(0,0,0))
        win.blit(te5,(200,600))
    
    
    if (man1.scoreplayer<man2.scoreplayer):
        te5=bfont.render('Result:Player 2 wins!!',1,(0,0,0))
        win.blit(te5,(200,600))
    
    
    if (man1.scoreplayer==man2.scoreplayer):
        if (t1>t2):
            te5=bfont.render('Result:Player 2 wins!!',1,(0,0,0))
            win.blit(te5,(200,600))
        
        
        if (t1<t2):
            te5=bfont.render('Result:Player 1 wins!!',1,(0,0,0))
            win.blit(te5,(200,600))        
    
    pygame.display.update()

def redrawgamewindow():
    if (c==0):
        bgwin()
        text=font.render('Score player 1: '+str(man1.scoreplayer),1,(0,0,0))
        win.blit(char,(man1.x,man1.y))
        win.blit(ship,(man1.xshi[0],49))
        win.blit(ship,(man1.xshi[1],189))
        win.blit(ship,(man1.xshi[2],329))
        win.blit(ship,(man1.xshi[3],469))
        win.blit(ship,(man1.xshi[4],609))
        scoredisplay()
        timedisplay()
        start=efont.render('START',1,(226,226,57))
        win.blit(start,(500,715))
        end=efont.render('END',1,(226,226,57))
        win.blit(end,(500,0))        
        pygame.display.update()
    
    
    if (c==1):
        bgwin()
        text=font.render('Score player 2: '+str(man2.scoreplayer),1,(0,0,0))
        win.blit(char1,(man2.x,man2.y))
        win.blit(ship,(man2.xshi[0],49))
        win.blit(ship,(man2.xshi[1],189))
        win.blit(ship,(man2.xshi[2],329))
        win.blit(ship,(man2.xshi[3],469))
        win.blit(ship,(man2.xshi[4],609))
        scoredisplay()
        timedisplay()
        start=efont.render('START',1,(226,226,57))
        win.blit(start,(500,0))
        end=efont.render('END',1,(226,226,57))
        win.blit(end,(500,715))    
        pygame.display.update()


#main loop
man1=player(10,700,3)
man2=player(800,-10,3)
font=pygame.font.SysFont('comicsans',30,True,True)
run=True
while run: 
    if (c==0):
        man1.shiploop()
    
    
    if (c==1):
        man2.shiploop()
    clock.tick(27)
    
    
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            run=False
    
    
    if (c==0) and fplayer1==True:
        keys=pygame.key.get_pressed()
        t1=pygame.time.get_ticks()
        t1-=t2
        
        if keys[pygame.K_LEFT] and man1.x > man1.vel:
            man1.x-=man1.vel
        
        if keys[pygame.K_RIGHT] and man1.x < scrnwidth-man1.width-man1.vel:
            man1.x+=man1.vel
        
        dcws1(man1.xshi[0],49) 
        dcws1(man1.xshi[1],189)
        dcws1(man1.xshi[2],329)
        dcws1(man1.xshi[3],469)
        dcws1(man1.xshi[4],609)
        dcwr1(170,560)
        dcwr1(20,560)
        dcwr1(320,560)
        dcwr1(470,560)
        dcwr1(620,560)
        dcwr1(770,560)
        dcwr1(170,280)
        dcwr1(20,280)
        dcwr1(320,280)
        dcwr1(470,280)
        dcwr1(620,280)
        dcwr1(770,280)
        dcwb1(390,420)
        dcwb1(40,420)
        dcwb1(140,420)
        dcwb1(610,420)
        dcwb1(800,420)
        dcwb1(10,140)
        dcwb1(110,140)
        dcwb1(310,140)
        dcwb1(410,140)
        dcwb1(610,140)
        dcwb1(790,140)
        
        
        if keys[pygame.K_UP] and man1.y > -5:
            man1.y-=man1.vel
            man1.scoresystem1()
        
        if keys[pygame.K_DOWN] and man1.y < 770-man1.width-man1.vel:
            man1.y+=man1.vel
        
        if man1.scoreplayer==man1.updatescore[8] and fplayer2==True:
            c=1
            f=0
        
        if man1.scoreplayer==man1.updatescore[8] and fplayer2==False:
            f1=0  
        
        if fplayer2==False and fplayer1==True and f1==0:
            rounds+=1
            for i in range(0,9):
                man1.updatescore[i]=man1.updatescore[i]+70
                man1.vis[i]=0
            man1.x=10
            man1.y=700
            man1.velship=man1.velship+3
            f1=1
        redrawgamewindow()
        
        if fplayer1==False and fplayer2==False:
            summarywindow()
    
    
    if (c==1) and fplayer2==True:
        t2=pygame.time.get_ticks()
        t2-=t1
        f=0
        keys=pygame.key.get_pressed()
        
        if keys[pygame.K_a] and man2.x > man2.vel:
            man2.x-=man2.vel
        
        if keys[pygame.K_d] and man2.x < scrnwidth-man2.width-man2.vel:
            man2.x+=man2.vel
        dcws2(man2.xshi[0],49)
        dcws2(man2.xshi[1],189)
        dcws2(man2.xshi[2],329)
        dcws2(man2.xshi[3],469)
        dcws2(man2.xshi[4],609)
        dcwr2(170,560)
        dcwr2(20,560)
        dcwr2(320,560)
        dcwr2(470,560)
        dcwr2(620,560)
        dcwr2(770,560)
        dcwr2(170,280)
        dcwr2(20,280)
        dcwr2(320,280)
        dcwr2(470,280)
        dcwr2(620,280)
        dcwr2(770,280)
        dcwb2(390,420)
        dcwb2(40,420)
        dcwb2(140,420)
        dcwb2(610,420)
        dcwb2(800,420)
        dcwb2(10,140)
        dcwb2(110,140)
        dcwb2(310,140)
        dcwb2(410,140)
        dcwb2(610,140)
        dcwb2(790,140)
        
        if keys[pygame.K_w] and man2.y > -10:
            man2.y-=man2.vel
        
        if keys[pygame.K_s] and man2.y < 780:
            man2.y+=man2.vel
            man2.scoresystem2()
        
        
        if man2.scoreplayer==man2.updatescore[8] and f==0 and fplayer1==True:
            rounds+=1
            #man1
            
            for i in range(0,9):
                man1.updatescore[i]=man1.updatescore[i]+70
                man1.vis[i]=0
            man1.x=10
            man1.y=700
            man1.velship=man1.velship+3
            #man2
            
            for i in range(0,9):
                man2.updatescore[i]=man2.updatescore[i]+70
                man2.vis[i]=0
            man2.x=800
            man2.y=-10
            man2.velship=man2.velship+3
            c=0
            f=1
        
        
        if man2.scoreplayer==man2.updatescore[8] and f==0 and fplayer1==False:
            rounds+=1
            for i in range(0,9):
                man2.updatescore[i]=man2.updatescore[i]+70
                man2.vis[i]=0
            man2.x=800
            man2.y=-10
            man2.velship=man2.velship+3
            c=1
            f=1
        
        redrawgamewindow()    
        
        if fplayer1==False and fplayer2==False:
            summarywindow()    
pygame.quit()