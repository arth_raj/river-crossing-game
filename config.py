scrnwidth=900
c=0
rounds=0
fplayer1=True
fplayer2=True
f=0 
f1=0
t1=0
t2=0
class player(object):
    def __init__(self,x,y,velship):
        self.x=x
        self.y=y
        self.width=64
        self.height=64
        self.velship=velship
        self.walkCount=0
        self.scoreplayer=0
        self.left=False
        self.right=False
        self.vel=10
        self.updatescore=[10,15,25,30,40,45,55,60,70]
        self.vis=[0,0,0,0,0,0,0,0,0]
        self.xshi=[10,100,210,150,400]
    def scoresystem1(self):
        if self.y<0 and self.vis[8]==0:
            self.vis[8]=1
            self.scoreplayer=self.updatescore[8]
            return
        if self.y+15<140 and self.vis[7]==0:
            self.vis[7]=1
            self.scoreplayer=self.updatescore[7]
            return
        if self.y+15<180 and self.vis[6]==0:
            self.vis[6]=1
            self.scoreplayer=self.updatescore[6]
            return
        if self.y+15 <280 and self.vis[5]==0:
            self.vis[5]=1
            self.scoreplayer=self.updatescore[5]
            return
        if self.y+15<320 and self.vis[4]==0:
            self.vis[4]=1
            self.scoreplayer=self.updatescore[4]
            return
        if self.y+15<420 and self.vis[3]==0:
            self.vis[3]=1
            self.scoreplayer=self.updatescore[3]
            return
        if self.y+15<460 and self.vis[2]==0:
            self.vis[2]=1
            self.scoreplayer=self.updatescore[2]
            return
        if self.y+15<560 and self.vis[1]==0:
            self.vis[1]=1
            self.scoreplayer=self.updatescore[1]
            return
        if self.y+15<600 and self.vis[0]==0:
            self.vis[0]=1
            self.scoreplayer=self.updatescore[0]
            return
    def scoresystem2(self):
        if self.y>685 and self.vis[8]==0:
            self.vis[8]=1
            self.scoreplayer=self.updatescore[8]
            return        
        if self.y>585 and self.vis[7]==0:
            self.vis[7]=1
            self.scoreplayer=self.updatescore[7]
            return        
        if self.y>545 and self.vis[6]==0:
            self.vis[6]=1
            self.scoreplayer=self.updatescore[6]
            return        
        if self.y>445 and self.vis[5]==0:
            self.vis[5]=1
            self.scoreplayer=self.updatescore[5]
            return
        if self.y>405 and self.vis[4]==0:
            self.vis[4]=1
            self.scoreplayer=self.updatescore[4]
            return
        if self.y>305 and self.vis[3]==0:
            self.vis[3]=1
            self.scoreplayer=self.updatescore[3]
            return
        if self.y>265 and self.vis[2]==0:
            self.vis[2]=1
            self.scoreplayer=self.updatescore[2]
            return
        if self.y>165 and self.vis[1]==0:
            self.vis[1]=1
            self.scoreplayer=self.updatescore[1]
            return
        if self.y>125 and self.vis[0]==0:
            self.vis[0]=1
            self.scoreplayer=self.updatescore[0]
            return            
    def shiploop(self):
        for i in range (0,5):
            self.xshi[i]+=self.velship
            if (self.xshi[i]>799):
                self.xshi[i]=0