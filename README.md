# ISS Assignment 3 
### Game Development using Python and PyGame 
## _**River_Crossing_Game**_
> _ARTH RAJ 
> 2019101094_

### Read the instructions and try out my game.  


### Gameplay and Rules :  

The game has multiple rounds. 

Player 1 is at bottom and has to reach the other end by avoiding all the obstacles and in minimum time.  
Player 2 starts at the top of the screen and has to reach the bottom by avoiding all the obstacles and in minimum time.

As the player crosses a band, he/she is rewarded points according to the number of obstacles in that band, the rules are :–   
+5 for fixed obstacles  
+10 for moving obstacles  

The player with the greater score wins the game, but if the scores are equal than the player taking less time to attain that score wins. 
In the subsquent round, the speed of the moving obstacles increases      

### Controls: 

Keyboard `Arrow Keys` to move up, down, left and right for `PLAYER 1`. 
Keyboard `WASD` to move up, down, left and right for  `PLAYER 2`.     

### How to install : 

1. Download the folder/copy it to your local device. 
1. Install python, pygame 
	1. Check this link to get them installed on linux – https://linuxize.com/post/how-to-install-python-3-7-on-ubuntu-18-04/ 
	1. Download PyGame – https://www.pygame.org/download.shtml 
	1. Further help - https://www.pygame.org/wiki/GettingStarted
1. Open the folder in Terminal and run : 
	1. python3 main.py 
1. Enjoy the Game.  

#### Attributes
In this game I have used `Python 3` and `PyGame`.

These are some resources I used for icons.
Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a 
		href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a 
		href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a 
		href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>  
Icons made by <a href="https://www.flaticon.com/authors/darius-dan" title="Darius Dan">Darius Dan</a> from <a 
		href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>  

For the game development i have used Visual Studio Code 